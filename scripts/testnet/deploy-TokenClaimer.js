const hre = require("hardhat");

const {
    TokenHolderRegisterDeployedAddress,
    OlympiaDeployedAddress,
    TokenClaimerDeployedAddress
} = require('../../src/config.json');

async function main() {
    let tokenHolderRegister = await loadTokenHolderRegister();

    let olympia = await loadOlympia();

    //let tokenClaimer = await deployTokenClaimer(tokenHolderRegister.address, olympia.address);
    // try { await verifyTokenClaimer(tokenClaimer.address, tokenHolderRegister.address, olympia.address); } catch (error) { console.log(`Error: ${error}`); }
    let tokenClaimer = await loadTokenClaimer();
    try { await verifyTokenClaimer(tokenClaimer.address, tokenHolderRegister.address, olympia.address); } catch (error) { console.log(`Error: ${error}`); }

    await allowTokenClaimer(tokenHolderRegister, tokenClaimer.address);
    // await excludeTokenClaimerFromFees(olympia, tokenClaimer.address);
    // await transferOlympia(olympia, tokenClaimer.address, await olympia.totalSupply());
}

async function loadTokenHolderRegister() {
    console.log("Loading TokenHolderRegister ...");
    let TokenHolderRegister = await hre.ethers.getContractFactory("contracts/TokenHolderRegister.sol:TokenHolderRegister");
    let tokenHolderRegister = await TokenHolderRegister.attach(TokenHolderRegisterDeployedAddress);
    console.log(`TokenHolderRegister loaded from: ${tokenHolderRegister.address}`);
    return tokenHolderRegister;
}

async function loadOlympia() {
    console.log("Loading Olympia ...");
    const Olympia = await hre.ethers.getContractFactory("contracts/Olympia.sol:Olympia");
    const olympia = await Olympia.attach(OlympiaDeployedAddress);
    console.log(`Olympia loaded from: ${olympia.address}`);
    return olympia;
}

async function deployTokenClaimer(tokenHolderRegisterAddress, olympiaAddress) {
    console.log("Deploying TokenClaimer ...");
    let TokenClaimer = await hre.ethers.getContractFactory("contracts/TokenClaimer.sol:TokenClaimer");
    let tokenClaimer = await TokenClaimer.deploy(tokenHolderRegisterAddress, olympiaAddress);
    await tokenClaimer.deployed();
    console.log(`TokenClaimer deployed to: ${tokenClaimer.address}`);
    return tokenClaimer;
}

async function loadTokenClaimer() {
    console.log("Loading TokenClaimer ...");
    const TokenClaimer = await hre.ethers.getContractFactory("contracts/TokenClaimer.sol:TokenClaimer");
    const tokenClaimer = await TokenClaimer.attach(TokenClaimerDeployedAddress);
    console.log(`TokenClaimer loaded from: ${tokenClaimer.address}`);
    return tokenClaimer;
}

async function verifyTokenClaimer(tokenClaimerAddress, tokenHolderRegisterAddress, olympiaAddress) {
    console.log("Verifying TokenClaimer ...");
    await hre.run("verify:verify", {
        contract: "contracts/TokenClaimer.sol:TokenClaimer",
        address: tokenClaimerAddress,
        constructorArguments: [
          tokenHolderRegisterAddress,
          olympiaAddress
        ]
    });
    console.log("TokenClaimer verified");
}

async function allowTokenClaimer(tokenHolderRegister, tokenClaimerAddress) {
  console.log("Allowing TokenClaimer to execute TokenHolderRegister restricted functions ...");
  await tokenHolderRegister.allow(tokenClaimerAddress, true);
  console.log("TokenClaimer allowed");
}

async function excludeTokenClaimerFromFees(olympia, tokenClaimerAddress) {
  console.log("Excluding TokenClaimer from fees ...");
  await olympia.excludeFromFees(tokenClaimerAddress, true);
  console.log("TokenClaimer excluded");
}

async function excludeTokenClaimerFromAntiWhale(olympia, tokenClaimerAddress) {
  console.log("Excluding TokenClaimer from anti whale ...");
  await olympia.setIsExcludedFromAntiWhale(tokenClaimerAddress, true);
  console.log("TokenClaimer excluded");
}

async function setMaxTransaction(olympia) {
  console.log("Setting Max Transaction ...");
  await olympia.setMaxTransaction(6);
  console.log("Max Transaction set");
}

async function transferOlympia(olympia, tokenClaimerAddress) {
  console.log("Transfering Olympia to TokenClaimer ...");
  await olympia.transfer(tokenClaimerAddress, await olympia.totalSupply());
  console.log("Olympia transfered");
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
