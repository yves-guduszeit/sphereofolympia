const hre = require("hardhat");

const {
    IterableMappingDeployedAddress,
    OlpTokenDeployedAddress
} = require('../../src/config.json');

async function main() {
  let iterableMapping = await loadIterableMapping();

  let olpToken = await loadOlpToken(iterableMapping.address);

  let tokenHolderRegister = await deployTokenHolderRegister();
  try { await verifyTokenHolderRegister(tokenHolderRegister.address); } catch (error) { console.log(`Error: ${error}`); }

  let tokenDelegator = await deployTokenDelegator(tokenHolderRegister.address, olpToken.address);
  try { await verifyTokenDelegator(tokenDelegator.address, tokenHolderRegister.address, olpToken.address); } catch (error) { console.log(`Error: ${error}`); }

  await allowTokenDelegator(tokenHolderRegister, tokenDelegator.address);
  await excludeTokenDelegatorFromFees(olpToken, tokenDelegator.address);
}

async function loadIterableMapping() {
    console.log("Loading IterableMapping ...");
    let IterableMapping = await hre.ethers.getContractFactory("contracts/OlpToken.sol:IterableMapping");
    let iterableMapping = await IterableMapping.attach(IterableMappingDeployedAddress);
    console.log(`IterableMapping loaded from: ${iterableMapping.address}`);
    return iterableMapping;
}

async function loadOlpToken(iterableMappingAddress) {
    console.log("Loading OlpToken ...");
    let OlpToken = await hre.ethers.getContractFactory("contracts/OlpToken.sol:OlpToken", {
        libraries: {
            IterableMapping: iterableMappingAddress
        }
    });
    let olpToken = await OlpToken.attach(OlpTokenDeployedAddress);
    console.log(`OlpToken loaded from: ${olpToken.address}`);
    return olpToken;
}

async function deployTokenHolderRegister()
{
    console.log("Deploying TokenHolderRegister ...");
    let TokenHolderRegister = await hre.ethers.getContractFactory("contracts/TokenHolderRegister.sol:TokenHolderRegister");
    let tokenHolderRegister = await TokenHolderRegister.deploy();
    await tokenHolderRegister.deployed();
    console.log(`TokenHolderRegister deployed to: ${tokenHolderRegister.address}`);
    return tokenHolderRegister;
}

async function verifyTokenHolderRegister(tokenHolderRegisterAddress) {
    console.log("Verifying TokenHolderRegister ...");
    await hre.run("verify:verify", {
        contract: "contracts/TokenHolderRegister.sol:TokenHolderRegister",
        address: tokenHolderRegisterAddress
    });
    console.log("TokenHolderRegister verified");
}

async function deployTokenDelegator(tokenHolderRegisterAddress, olpTokenAddress)
{
    console.log("Deploying TokenDelegator ...");
    let TokenDelegator = await hre.ethers.getContractFactory("contracts/TokenDelegator.sol:TokenDelegator");
    let tokenDelegator = await TokenDelegator.deploy(tokenHolderRegisterAddress, olpTokenAddress);
    await tokenDelegator.deployed();
    console.log(`TokenDelegator deployed to: ${tokenDelegator.address}`);
    return tokenDelegator;
}

async function verifyTokenDelegator(tokenDelegatorAddress, tokenHolderRegisterAddress, olpTokenAddress) {
    console.log("Verifying TokenDelegator ...");
    await hre.run("verify:verify", {
        contract: "contracts/TokenDelegator.sol:TokenDelegator",
        address: tokenDelegatorAddress,
        constructorArguments: [
          tokenHolderRegisterAddress,
          olpTokenAddress
        ]
    });
    console.log("TokenDelegator verified");
}

async function allowTokenDelegator(tokenHolderRegister, tokenDelegatorAddress) {
    console.log("Allowing TokenDelegator to execute TokenHolderRegister restricted functions ...");
    await tokenHolderRegister.allow(tokenDelegatorAddress, true);
    console.log("TokenDelegator allowed");
}

async function excludeTokenDelegatorFromFees(olpToken, tokenDelegatorAddress) {
    console.log("Excluding TokenDelegator from fees ...");
    await olpToken.excludeFromFees(tokenDelegatorAddress, true);
    console.log("TokenDelegator excluded");
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
