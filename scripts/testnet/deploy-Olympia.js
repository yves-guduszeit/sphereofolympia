const hre = require("hardhat");

const {
    IterableMappingDeployedAddress,
    UniswapV2Router02DeployedAddress,
    ReflectionDistributorDeployedAddress,
    DividendTrackerDeployedAddress,
    OlympiaDeployedAddress
} = require('../../src/config.json');

const {
    TeamWallet,
    ProviderWallet,
    MarketingWallet
} = require('../../secrets.json');

async function main() {
    //let reflectionDistributor = await deployReflectionDistributor();
    // try { await verifyReflectionDistributor(reflectionDistributor.address); } catch (error) { console.log(`Error: ${error}`); }
    let reflectionDistributor = await loadReflectionDistributor();
    try { await verifyReflectionDistributor(reflectionDistributor.address); } catch (error) { console.log(`Error: ${error}`); }

    //let olympia = await deployOlympia(UniswapV2Router02DeployedAddress, TeamWallet, ProviderWallet, MarketingWallet, reflectionDistributor.address);
    //try { await verifyOlympia(olympia.address, UniswapV2Router02DeployedAddress, TeamWallet, ProviderWallet, MarketingWallet, reflectionDistributor.address); } catch (error) { console.log(`Error: ${error}`); }
    let olympia = await loadOlympia();
    try { await verifyOlympia(olympia.address, UniswapV2Router02DeployedAddress, TeamWallet, ProviderWallet, MarketingWallet, reflectionDistributor.address); } catch (error) { console.log(`Error: ${error}`); }
    
    //await allowOlympia(reflectionDistributor, olympia.address);
    // await addLiquidity(olympia, await getUniswapV2Router02Contract(), signer.address);

    // // let iterableMapping = await deployIterableMapping();
    // // try { await verifyIterableMapping(iterableMapping.address); } catch (error) { console.log(`Error: ${error}`); }
    // let iterableMapping = await loadIterableMapping();

    // let dividendTracker = await deployDividendTracker(iterableMapping.address);
    // // try { await verifyDividendTracker(dividendTracker.address, iterableMapping.address); } catch (error) { console.log(`Error: ${error}`); }
    // // let dividendTracker = await loadDividendTracker(iterableMapping);

    // let olympia = await deployOlympia(UniswapV2Router02DeployedAddress, TeamWallet, ProviderWallet, MarketingWallet, dividendTracker.address);
    // // try { await verifyOlympia(olympia.address, UniswapV2Router02DeployedAddress, TeamWallet, ProviderWallet, MarketingWallet, dividendTracker.address); } catch (error) { console.log(`Error: ${error}`); }
    // // let olympia = await loadOlympia();
}

async function deployReflectionDistributor() {
    console.log("Deploying ReflectionDistributor ...");
    let ReflectionDistributor = await hre.ethers.getContractFactory("contracts/ReflectionDistributor.sol:ReflectionDistributor");
    let reflectionDistributor = await ReflectionDistributor.deploy();
    await reflectionDistributor.deployed();
    console.log(`ReflectionDistributor deployed to: ${reflectionDistributor.address}`);
    return reflectionDistributor;
}

async function loadReflectionDistributor() {
    console.log("Loading ReflectionDistributor ...");
    const ReflectionDistributor = await hre.ethers.getContractFactory("contracts/ReflectionDistributor.sol:ReflectionDistributor");
    const reflectionDistributor = await ReflectionDistributor.attach(ReflectionDistributorDeployedAddress);
    console.log(`ReflectionDistributor loaded from: ${reflectionDistributor.address}`);
    return reflectionDistributor;
}

async function verifyReflectionDistributor(reflectionDistributorAddress) {
    console.log("Verifying ReflectionDistributor ...");
    await hre.run("verify:verify", {
        contract: "contracts/ReflectionDistributor.sol:ReflectionDistributor",
        address: reflectionDistributorAddress
    });
    console.log("ReflectionDistributor verified");
}

async function deployOlympia(routerAddress, teamWallet, providerWallet, marketingWallet, reflectionDistributorAddress) {
    console.log("Deploying Olympia ...");
    let Olympia = await hre.ethers.getContractFactory("contracts/Olympia.sol:Olympia");
    let olympia = await Olympia.deploy(routerAddress, teamWallet, providerWallet, marketingWallet, reflectionDistributorAddress);
    await olympia.deployed();
    console.log(`Olympia deployed to: ${olympia.address}`);
    return olympia;
}

async function loadOlympia() {
    console.log("Loading Olympia ...");
    const Olympia = await hre.ethers.getContractFactory("contracts/Olympia.sol:Olympia");
    const olympia = await Olympia.attach(OlympiaDeployedAddress);
    console.log(`Olympia loaded from: ${olympia.address}`);
    return olympia;
}

async function verifyOlympia(olympiaAddress, routerAddress, teamWallet, providerWallet, marketingWallet, reflectionDistributorAddress) {
    console.log("Verifying Olympia ...");
    await hre.run("verify:verify", {
        contract: "contracts/Olympia.sol:Olympia",
        address: olympiaAddress,
        constructorArguments: [
            routerAddress,
            teamWallet,
            providerWallet,
            marketingWallet,
            reflectionDistributorAddress
        ]
    });
    console.log("Olympia verified");
}

async function allowOlympia(reflectionDistributor, olympiaAddress) {
    console.log("Allowing Olympia to execute ReflectionDistributor restricted functions ...");
    await reflectionDistributor.allow(olympiaAddress, true);
    console.log("Olympia allowed");
}

// async function addLiquidity(olympia, router, signerAddress) {
//     console.log("Adding liquidity ...");
//     await olympia.approve(router.address, await olympia.totalSupply());
//     let liquidityAddress = await router.addLiquidityETH(
//         olympia.address,
//         await olympia.totalSupply() / 2,
//         await olympia.totalSupply() / 2,
//         1000000000000000,
//         signerAddress,
//         Date.now() + 1000 * 60 * 5);
//     console.log("Liquidity added");
// }

// async function deployIterableMapping() {
//     console.log("Deploying IterableMapping ...");
//     let IterableMapping = await hre.ethers.getContractFactory("contracts/libraries/IterableMapping.sol:IterableMapping");
//     let iterableMapping = await IterableMapping.deploy();
//     await iterableMapping.deployed();
//     console.log(`IterableMapping deployed to: ${iterableMapping.address}`);
//     return iterableMapping;
// }

// async function loadIterableMapping() {
//     console.log("Loading IterableMapping ...");
//     let IterableMapping = await hre.ethers.getContractFactory("contracts/libraries/IterableMapping.sol:IterableMapping");
//     let iterableMapping = await IterableMapping.attach(IterableMappingDeployedAddress);
//     console.log(`IterableMapping loaded from: ${iterableMapping.address}`);
//     return iterableMapping;
// }

// async function verifyIterableMapping(iterableMappingAddress) {
//     console.log("Verifying IterableMapping ...");
//     await hre.run("verify:verify", {
//         contract: "contracts/libraries/IterableMapping.sol:IterableMapping",
//         address: iterableMappingAddress
//     });
//     console.log("IterableMapping verified");
// }

// async function deployDividendTracker(iterableMappingAddress) {
//     console.log("Deploying DividendTracker ...");
//     let DividendTracker = await hre.ethers.getContractFactory("contracts/DividendTracker.sol:DividendTracker", {
//         libraries: {
//             IterableMapping: iterableMappingAddress
//         }
//     });
//     let dividendTracker = await DividendTracker.deploy();
//     await dividendTracker.deployed();
//     console.log(`DividendTracker deployed to: ${dividendTracker.address}`);
//     return dividendTracker;
// }

// async function loadDividendTracker(iterableMapping) {
//     console.log("Loading DividendTracker ...");
//     const DividendTracker = await hre.ethers.getContractFactory("contracts/DividendTracker.sol:DividendTracker", {
//         libraries: {
//             IterableMapping: iterableMapping.address
//         }
//     });
//     const dividendTracker = await DividendTracker.attach(DividendTrackerDeployedAddress);
//     console.log(`DividendTracker loaded from: ${dividendTracker.address}`);
//     return dividendTracker;
// }

// async function verifyDividendTracker(dividendTrackerAddress, iterableMappingAddress) {
//     console.log("Verifying DividendTracker ...");
//     await hre.run("verify:verify", {
//         contract: "contracts/DividendTracker.sol:DividendTracker",
//         address: dividendTrackerAddress,
//         libraries: {
//             IterableMapping: iterableMappingAddress
//         }
//     });
//     console.log("DividendTracker verified");
// }

// async function deployOlympia(routerAddress, teamWallet, providerWallet, marketingWallet, dividendTrackerAddress) {
//     console.log("Deploying Olympia ...");
//     let Olympia = await hre.ethers.getContractFactory("contracts/Olympia.sol:Olympia");
//     let olympia = await Olympia.deploy(routerAddress, teamWallet, providerWallet, marketingWallet, dividendTrackerAddress);
//     await olympia.deployed();
//     console.log(`Olympia deployed to: ${olympia.address}`);
//     return olympia;
// }

// async function loadOlympia() {
//     console.log("Loading Olympia ...");
//     const Olympia = await hre.ethers.getContractFactory("contracts/Olympia.sol:Olympia");
//     const olympia = await Olympia.attach(OlympiaDeployedAddress);
//     console.log(`Olympia loaded from: ${olympia.address}`);
//     return olympia;
// }

// async function verifyOlympia(olympiaAddress, routerAddress, teamWallet, providerWallet, marketingWallet, dividendTrackerAddress) {
//     console.log("Verifying Olympia ...");
//     await hre.run("verify:verify", {
//         contract: "contracts/Olympia.sol:Olympia",
//         address: olympiaAddress,
//         constructorArguments: [
//             routerAddress,
//             teamWallet,
//             providerWallet,
//             marketingWallet,
//             dividendTrackerAddress
//         ]
//     });
//     console.log("Olympia verified");
// }

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
