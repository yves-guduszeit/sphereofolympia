const hre = require("hardhat");

const {
    IterableMappingDeployedAddress,
    OlpTokenDeployedAddress
} = require('../../src/config.json');

async function main() {
    let iterableMapping = await deployIterableMapping();
    try { await verifyIterableMapping(iterableMapping.address); } catch (error) { console.log(`Error: ${error}`); }
    //let iterableMapping = await loadIterableMapping();

    let olpToken = await deployOlpToken(iterableMapping.address);
    try { await verifyOlpToken(olpToken.address, iterableMapping.address); } catch (error) { console.log(`Error: ${error}`); }
    //let olpToken = await loadOlpToken(iterableMapping);

    await removeCooldownTime(olpToken);
}

async function deployIterableMapping() {
    console.log("Deploying IterableMapping ...");
    let IterableMapping = await hre.ethers.getContractFactory("contracts/libraries/IterableMapping.sol:IterableMapping");
    let iterableMapping = await IterableMapping.deploy();
    await iterableMapping.deployed();
    console.log(`IterableMapping deployed to: ${iterableMapping.address}`);
    return iterableMapping;
}

async function loadIterableMapping() {
    console.log("Loading IterableMapping ...");
    let IterableMapping = await hre.ethers.getContractFactory("contracts/libraries/IterableMapping.sol:IterableMapping");
    let iterableMapping = await IterableMapping.attach(IterableMappingDeployedAddress);
    console.log(`IterableMapping loaded from: ${iterableMapping.address}`);
    return iterableMapping;
}

async function verifyIterableMapping(iterableMappingAddress) {
    console.log("Verifying IterableMapping ...");
    await hre.run("verify:verify", {
        contract: "contracts/OlpToken.sol:IterableMapping",
        address: iterableMappingAddress
    });
    console.log("IterableMapping verified");
}

async function deployOlpToken(iterableMappingAddress) {
    console.log("Deploying OlpToken ...");
    let OlpToken = await hre.ethers.getContractFactory("contracts/OlpToken.sol:OlpToken", {
        libraries: {
            IterableMapping: iterableMappingAddress
        }
    });
    let olpToken = await OlpToken.deploy();
    await olpToken.deployed();
    console.log(`OlpToken deployed to: ${olpToken.address}`);
    return olpToken;
}

async function loadOlpToken(iterableMappingAddress) {
    console.log("Loading OlpToken ...");
    const OlpToken = await hre.ethers.getContractFactory("contracts/OlpToken.sol:OlpToken", {
        libraries: {
            IterableMapping: iterableMappingAddress
        }
    });
    const olpToken = await OlpToken.attach(OlpTokenDeployedAddress);
    console.log(`OlpToken loaded from: ${olpToken.address}`);
    return olpToken;
}

async function verifyOlpToken(olpTokenAddress, iterableMappingAddress) {
    console.log("Verifying OlpToken ...");
    await hre.run("verify:verify", {
        contract: "contracts/OlpToken.sol:OlpToken",
        address: olpTokenAddress,
        libraries: {
            IterableMapping: iterableMappingAddress
        }
    });
    console.log("OlpToken verified");
}

async function removeCooldownTime(olpToken) {
    console.log("Removing Cooldown time ...");
    await olpToken.setTransactionCooldownTime(0);
    console.log("Cooldown time removed");
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
