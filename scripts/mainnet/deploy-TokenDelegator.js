const hre = require("hardhat");

async function main() {
  console.log("Deploying TokenHolderRegister ...");
  const TokenHolderRegister = await hre.ethers.getContractFactory("TokenHolderRegister");
  const tokenHolderRegister = await TokenHolderRegister.deploy();
  await tokenHolderRegister.deployed();
  console.log("TokenHolderRegister deployed to:", tokenHolderRegister.address);

  console.log("Loading OLPTOKEN ...");
  const V1Token = await hre.ethers.getContractFactory("V1Token");
  const v1Token = await V1Token.attach("0x79395a77c8ddbefb74f242242960cac7368fd64e");
  console.log("OLPTOKEN deployed to:", v1Token.address);

  console.log("Deploying TokenDelegator ...");
  const TokenDelegator = await hre.ethers.getContractFactory("TokenDelegator");
  const tokenDelegator = await TokenDelegator.deploy(tokenHolderRegister.address, v1Token.address);
  await tokenDelegator.deployed();
  console.log("TokenDelegator deployed to:", tokenDelegator.address);
  
  await tokenHolderRegister.allow(tokenDelegator.address, true);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
