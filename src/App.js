import './App.css';
import { ethers } from 'ethers';
import TokenClaimer from './artifacts/contracts/TokenClaimer.sol/TokenClaimer.json';

const {
  TokenClaimerDeployedAddress
} = require('./config.json');

function App() {
  async function requestAccount() {
    await window.ethereum.request({ method: 'eth_requestAccounts' });
  }

  async function getSigner() {
    await requestAccount();
    let provider = new ethers.providers.Web3Provider(window.ethereum);
    return provider.getSigner();
  }

  async function getTokenClaimerContract() {
    let signer = await getSigner();
    return new ethers.Contract(TokenClaimerDeployedAddress, TokenClaimer.abi, signer);
  }

  async function claimV2Tokens() {
    if (typeof window.ethereum !== 'undefined') {
      let tokenClaimerContract = await getTokenClaimerContract();
      let signer = await getSigner();
      let signerAddress = await signer.getAddress();
      let transation = await tokenClaimerContract.claimV2Tokens(signerAddress);
      await transation.wait();
      console.log(`V2 tokens claimed successfully from ${TokenClaimerDeployedAddress}`);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
          <button onClick={claimV2Tokens}>CLAIM YOUR $SOO</button>
      </header>
    </div>
  );
}

export default App;
