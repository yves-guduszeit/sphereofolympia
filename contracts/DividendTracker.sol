// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "./Allowable.sol";
import "./DividendPayingToken.sol";
import "./libraries/IterableMapping.sol";

contract DividendTracker is Allowable, DividendPayingToken {
    using SafeMath for uint256;
    using SafeMathInt for int256;
    using IterableMapping for IterableMapping.Map;

    IterableMapping.Map private _tokenHoldersMap;
    uint256 public _lastProcessedIndex;

    mapping (address => bool) public _excludedFromDividends;
    mapping (address => uint256) public _lastClaimTimes;

    uint256 public _claimWait;
    uint256 public _minimumTokenBalanceForDividends;

    event ExcludeFromDividends(address indexed account);
    event ClaimWaitUpdated(uint256 indexed newValue, uint256 indexed oldValue);
    event Claim(address indexed account, uint256 amount, bool indexed automatic);

    constructor() DividendPayingToken("Dividend_Tracker", "Olympia_Dividend_Tracker") {
    	_claimWait = 43_200;
        _minimumTokenBalanceForDividends = 100_000_000 * 10 ** 18;
    }

    function _transfer(address, address, uint256) internal override pure {
        require(false, "DividendTracker: No transfers allowed");
    }

    function withdrawDividend() public override pure {
        require(false, "DividendTracker: WithdrawDividend disabled. Use the 'claim' function on the main OLP contract.");
    }

    function excludeFromDividends(address account, bool enabled) external onlyAllowed {
    	require(_excludedFromDividends[account] != enabled, "DividendTracker: Account is already the value of 'enabled'");
    	_excludedFromDividends[account] = enabled;

    	_setBalance(account, 0);
    	_tokenHoldersMap.remove(account);

    	emit ExcludeFromDividends(account);
    }

    function updateClaimWait(uint256 newClaimWait) external onlyOwner {
        require(newClaimWait >= 3_600 && newClaimWait <= 86_400, "DividendTracker: _claimWait must be updated to between 1 and 24 hours");
        require(newClaimWait != _claimWait, "DividendTracker: Cannot update _claimWait to same value");
        emit ClaimWaitUpdated(newClaimWait, _claimWait);
        _claimWait = newClaimWait;
    }

    function getClaimWait() external view returns(uint256) {
        return _claimWait;
    }

    function getLastProcessedIndex() external view returns(uint256) {
    	return _lastProcessedIndex;
    }

    function getNumberOfTokenHolders() external view returns(uint256) {
        return _tokenHoldersMap.keys.length;
    }
    
    function setMinimumTokenBalanceForDividends(uint256 balance) external onlyOwner {
        _minimumTokenBalanceForDividends = balance * 10 ** 18;
    }

    function getAccount(address _account) public view returns (address account, int256 index, int256 iterationsUntilProcessed, uint256 withdrawableDividends, uint256 totalDividends, uint256 lastClaimTime, uint256 nextClaimTime, uint256 secondsUntilAutoClaimAvailable) {
        account = _account;

        index = _tokenHoldersMap.getIndexOfKey(account);

        iterationsUntilProcessed = -1;

        if (index >= 0) {
            if (uint256(index) > _lastProcessedIndex) {
                iterationsUntilProcessed = index.sub(int256(_lastProcessedIndex));
            }
            else {
                uint256 processesUntilEndOfArray = _tokenHoldersMap.keys.length > _lastProcessedIndex ? _tokenHoldersMap.keys.length.sub(_lastProcessedIndex) : 0;
                iterationsUntilProcessed = index.add(int256(processesUntilEndOfArray));
            }
        }

        withdrawableDividends = withdrawableDividendOf(account);
        totalDividends = accumulativeDividendOf(account);

        lastClaimTime = _lastClaimTimes[account];

        nextClaimTime = lastClaimTime > 0 ? lastClaimTime.add(_claimWait) : 0;

        secondsUntilAutoClaimAvailable = nextClaimTime > block.timestamp ? nextClaimTime.sub(block.timestamp) : 0;
    }

    function getAccountAtIndex(uint256 index) public view returns (address, int256, int256, uint256, uint256, uint256, uint256, uint256) {
    	if (index >= _tokenHoldersMap.size()) {
            return (0x0000000000000000000000000000000000000000, -1, -1, 0, 0, 0, 0, 0);
        }

        address account = _tokenHoldersMap.getKeyAtIndex(index);

        return getAccount(account);
    }

    function canAutoClaim(uint256 lastClaimTime) private view returns (bool) {
    	if (lastClaimTime > block.timestamp) {
    		return false;
    	}

    	return block.timestamp.sub(lastClaimTime) >= _claimWait;
    }

    function setBalance(address payable account, uint256 newBalance) external onlyAllowed {
    	if (_excludedFromDividends[account]) {
    		return;
    	}

    	if (newBalance >= _minimumTokenBalanceForDividends) {
            _setBalance(account, newBalance);
    		_tokenHoldersMap.set(account, newBalance);
    	} else {
            _setBalance(account, 0);
    		_tokenHoldersMap.remove(account);
    	}

    	processAccount(account, true);
    }

    function process(uint256 gas) public returns (uint256, uint256, uint256) {
    	uint256 numberOfTokenHolders = _tokenHoldersMap.keys.length;

    	if (numberOfTokenHolders == 0) {
    		return (0, 0, _lastProcessedIndex);
    	}

    	uint256 lastProcessedIndex = _lastProcessedIndex;

    	uint256 gasUsed = 0;

    	uint256 gasLeft = gasleft();

    	uint256 iterations = 0;
    	uint256 claims = 0;

    	while (gasUsed < gas && iterations < numberOfTokenHolders) {
    		lastProcessedIndex++;

    		if (lastProcessedIndex >= _tokenHoldersMap.keys.length) {
    			lastProcessedIndex = 0;
    		}

    		address account = _tokenHoldersMap.keys[lastProcessedIndex];

    		if (canAutoClaim(_lastClaimTimes[account])) {
    			if (processAccount(payable(account), true)) {
    				claims++;
    			}
    		}

    		iterations++;

    		uint256 newGasLeft = gasleft();

    		if (gasLeft > newGasLeft) {
    			gasUsed = gasUsed.add(gasLeft.sub(newGasLeft));
    		}

    		gasLeft = newGasLeft;
    	}

    	_lastProcessedIndex = lastProcessedIndex;

    	return (iterations, claims, _lastProcessedIndex);
    }

    function processAccount(address payable account, bool automatic) public onlyAllowed returns (bool) {
        uint256 amount = _withdrawDividendOfUser(account);

    	if (amount > 0) {
    		_lastClaimTimes[account] = block.timestamp;
            emit Claim(account, amount, automatic);
    		return true;
    	}

    	return false;
    }
}
