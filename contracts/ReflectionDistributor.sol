//SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "./interfaces/IReflectionDistributor.sol";
import "./libraries/SafeMath.sol";
import "./Allowable.sol";

contract ReflectionDistributor is Allowable, IReflectionDistributor {
    using SafeMath for uint256;
    
    struct Share {
        uint256 amount;
        uint256 totalExcluded;
        uint256 totalRealised;
    }
    
    address[] private _shareholders;
    mapping (address => uint256) private _shareholderIndexes;
    mapping (address => uint256) private _shareholderClaims;
    mapping (address => Share) public _shares;
    
    uint256 public _totalShares;
    uint256 public _totalDividends;
    uint256 public _totalDistributed;
    uint256 public _dividendsPerShare;
    uint256 public _dividendsPerShareAccuracyFactor = 10 ** 36;
    
    uint256 public _minPeriod = 12 * 60 * 60;
    uint256 public _minDistribution = 100_000_000 * 10 ** 18;
    
    uint256 private _currentIndex;
    
    receive() external payable {}

    function setDistributionCriteria(uint256 minPeriod, uint256 minDistribution) external onlyAllowed {
        _minPeriod = minPeriod;
        _minDistribution = minDistribution * 10 ** 18;
    }
    
    function setShare(address shareholder, uint256 amount) external override onlyAllowed {
        if (_shares[shareholder].amount > 0) {
            _distributeDividends(shareholder);
        }

        if (amount > 0 && _shares[shareholder].amount == 0) {
            _addShareholder(shareholder);
        }
        else if (amount == 0 && _shares[shareholder].amount > 0) {
            _removeShareholder(shareholder);
        }

        _totalShares = _totalShares.sub(_shares[shareholder].amount).add(amount);
        _shares[shareholder].amount = amount;
        _shares[shareholder].totalExcluded = _getCumulativeDividends(_shares[shareholder].amount);
    }

    function process(uint256 gas) external override onlyAllowed {
        uint256 shareholderCount = _shareholders.length;

        if (shareholderCount == 0) {
            return;
        }

        uint256 gasUsed = 0;
        uint256 gasLeft = gasleft();
        uint256 iterations = 0;

        while (gasUsed < gas && iterations < shareholderCount) {
            if (_currentIndex >= shareholderCount) {
                _currentIndex = 0;
            }

            if (_shouldDistribute(_shareholders[_currentIndex])) {
                _distributeDividends(_shareholders[_currentIndex]);
            }

            gasUsed = gasUsed.add(gasLeft.sub(gasleft()));
            gasLeft = gasleft();
            _currentIndex++;
            iterations++;
        }
    }
    
    function _shouldDistribute(address shareholder) private view returns (bool) {
        return _shareholderClaims[shareholder] + _minPeriod < block.timestamp && _getUnpaidEarnings(shareholder) > _minDistribution;
    }
    
    function _getUnpaidEarnings(address shareholder) private view returns (uint256) {
        if (_shares[shareholder].amount == 0) {
            return 0;
        }

        uint256 shareholderTotalDividends = _getCumulativeDividends(_shares[shareholder].amount);
        uint256 shareholderTotalExcluded = _shares[shareholder].totalExcluded;

        if (shareholderTotalDividends <= shareholderTotalExcluded) {
            return 0;
        }

        return shareholderTotalDividends.sub(shareholderTotalExcluded);
    }
    
    function _getCumulativeDividends(uint256 share) private view returns (uint256) {
        return share.mul(_dividendsPerShare).div(_dividendsPerShareAccuracyFactor);
    }
    
    function _addShareholder(address shareholder) private {
        _shareholderIndexes[shareholder] = _shareholders.length;
        _shareholders.push(shareholder);
    }
    
    function _removeShareholder(address shareholder) private {
        _shareholders[_shareholderIndexes[shareholder]] = _shareholders[_shareholders.length - 1];
        _shareholderIndexes[_shareholders[_shareholders.length - 1]] = _shareholderIndexes[shareholder];
        _shareholders.pop();
    }
    
    function _distributeDividends(address shareholder) private {
        if (_shares[shareholder].amount == 0) {
            return;
        }

        uint256 amount = _getUnpaidEarnings(shareholder);
        if (amount > 0) {
            _totalDistributed = _totalDistributed.add(amount);
            (bool successShareholder, /* bytes memory data */) = payable(shareholder).call{value: amount, gas: 30_000}("");
            require(successShareholder, "ReflectionDistributor: Provider receiver rejected ETH transfer");
            _shareholderClaims[shareholder] = block.timestamp;
            _shares[shareholder].totalRealised = _shares[shareholder].totalRealised.add(amount);
            _shares[shareholder].totalExcluded = _getCumulativeDividends(_shares[shareholder].amount);
        }
    }
}
