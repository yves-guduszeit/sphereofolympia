// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./libraries/SafeMath.sol";
import "./libraries/SafeMathUint.sol";
import "./libraries/SafeMathInt.sol";
import "./interfaces/IDividendPayingTokenInterface.sol";
import "./interfaces/IDividendPayingTokenOptionalInterface.sol";

contract DividendPayingToken is ERC20, IDividendPayingTokenInterface, IDividendPayingTokenOptionalInterface {
    using SafeMath for uint256;
    using SafeMathUint for uint256;
    using SafeMathInt for int256;

    uint256 constant internal _magnitude = 2 ** 128;

    uint256 internal _magnifiedDividendPerShare;

    mapping(address => int256) internal _magnifiedDividendCorrections;
    mapping(address => uint256) internal _withdrawnDividends;

    uint256 public _totalDividendsDistributed;

    constructor(string memory name, string memory symbol) ERC20(name, symbol) {
    }

    receive() external payable {
        distributeDividends();
    }

    function distributeDividends() public override payable {
        require(totalSupply() > 0, "DividendPayingToken: Total supply should not be empty");

        if (msg.value > 0) {
            _magnifiedDividendPerShare = _magnifiedDividendPerShare.add((msg.value).mul(_magnitude) / totalSupply());
            emit DividendsDistributed(msg.sender, msg.value);

            _totalDividendsDistributed = _totalDividendsDistributed.add(msg.value);
        }
    }

    function withdrawDividend() public virtual override {
        _withdrawDividendOfUser(payable(msg.sender));
    }

    function _withdrawDividendOfUser(address payable user) internal returns (uint256) {
        uint256 _withdrawableDividend = withdrawableDividendOf(user);
        if (_withdrawableDividend > 0) {
            _withdrawnDividends[user] = _withdrawnDividends[user].add(_withdrawableDividend);
            emit DividendWithdrawn(user, _withdrawableDividend);
            (bool success,) = user.call{value: _withdrawableDividend, gas: 3000}("");

            if (!success) {
                _withdrawnDividends[user] = _withdrawnDividends[user].sub(_withdrawableDividend);
                return 0;
            }

            return _withdrawableDividend;
        }

        return 0;
    }

    function getTotalDividendsDistributed() external view returns (uint256) {
        return _totalDividendsDistributed;
    }

    function dividendOf(address owner) public view override returns(uint256) {
        return withdrawableDividendOf(owner);
    }

    function withdrawableDividendOf(address owner) public view override returns(uint256) {
        return accumulativeDividendOf(owner).sub(_withdrawnDividends[owner]);
    }

    function withdrawnDividendOf(address owner) public view override returns(uint256) {
        return _withdrawnDividends[owner];
    }

    function accumulativeDividendOf(address owner) public view override returns(uint256) {
        return _magnifiedDividendPerShare.mul(balanceOf(owner)).toInt256Safe()
            .add(_magnifiedDividendCorrections[owner]).toUint256Safe() / _magnitude;
    }

    function _transfer(address from, address to, uint256 value) internal virtual override {
        require(false, "DividendPayingToken: Should not perform any transfer");

        int256 _magCorrection = _magnifiedDividendPerShare.mul(value).toInt256Safe();
        _magnifiedDividendCorrections[from] = _magnifiedDividendCorrections[from].add(_magCorrection);
        _magnifiedDividendCorrections[to] = _magnifiedDividendCorrections[to].sub(_magCorrection);
    }

    function _mint(address account, uint256 value) internal override {
        super._mint(account, value);

        _magnifiedDividendCorrections[account] = _magnifiedDividendCorrections[account]
            .sub( (_magnifiedDividendPerShare.mul(value)).toInt256Safe() );
    }

    function _burn(address account, uint256 value) internal override {
        super._burn(account, value);

        _magnifiedDividendCorrections[account] = _magnifiedDividendCorrections[account]
            .add( (_magnifiedDividendPerShare.mul(value)).toInt256Safe() );
    }

    function _setBalance(address account, uint256 newBalance) internal {
        uint256 currentBalance = balanceOf(account);

        if (newBalance > currentBalance) {
            uint256 mintAmount = newBalance.sub(currentBalance);
            _mint(account, mintAmount);
        } else if (newBalance < currentBalance) {
            uint256 burnAmount = currentBalance.sub(newBalance);
            _burn(account, burnAmount);
        }
    }
}
