/*
Olympia

Total Supply:
    100,000,000,000 $OLP

Taxes:
    Buy Tax: 12.0%
        2.0% Auto Liquidity
        3.0% BNB Rewards
        3.0% Marketing
        2.0% Team
        2.0% Provider

    Sell Tax: 14.0%
        2.0% Auto Liquidity
        3.0% BNB Rewards
        5.0% Marketing
        2.0% Team
        2.0% Provider
    
Tokenomics:
    0.2% Max Buy Tx ( 200,000,000 $OLP )
    0.2% Max Sell Tx ( 200,000,000 $OLP )
    0.6% Max Bag ( 600,000,000 $OLP )
    10 Seconds TX Cooldown ( Buy & Sell )

Anti-Sniper Protocols:
    Manual Blacklist Function
    Max Wallet Amount
    Max Transaction Amount
    
 *
 */

//SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

import "./interfaces/IDEXFactory.sol";
import "./Auth.sol";
import "./DividendsDistributor.sol";

/* Token contract */
contract Olympia is IBEP20, Auth {
    using SafeMath for uint256;

    struct Fees {
        uint256 liquidityFeesPerThousand;
        uint256 marketingFeesPerThousand;
        uint256 teamFeesPerThousand;
        uint256 providerFeesPerThousand;
        uint256 reflectionFeesPerThousand;
    }
    
    // Addresses
    address private _teamWallet = 0xBAA83814E5405DCCd05dDF800ec74258981B7Af7;
    address private _providerWallet = 0xBAA83814E5405DCCd05dDF800ec74258981B7Af7;
    address private _deadWallet = 0x000000000000000000000000000000000000dEaD;
    address private _zeroWallet = address(0);
    
    // These are owner by default
    address public _autoLiquidityReceiver;
    address public _marketingFeeReceiver;
    
    // Name and symbol
    string constant _name = "Olympia";
    string constant _symbol = "OLP";
    uint8 constant _decimals = 18;
    
    // Total supply
    uint256 private _totalSupply = 100_000_000_000 * (10 ** _decimals);
    
    // Max wallet and transaction
    uint256 public _maxWalletToken = _totalSupply * 6 / 1_000; // 0.6%
    uint256 public _maxBuyTransactionAmount = _totalSupply * 2 / 1_000; // 0.2%
    uint256 public _maxSellTransactionAmount = _totalSupply * 2 / 1_000; // 0.2%
    
    // Mappings
    mapping (address => uint256) private _balances;
    mapping (address => mapping (address => uint256)) private _allowances;
    mapping (address => bool) private _isFeesExempt;
    mapping (address => bool) private _isTransactionLimitExempt;
    mapping (address => bool) private _isDividendsExempt;
    mapping (address => bool) private _isTimelockExempt;
    mapping (address => bool) private _isBlacklisted;
    
    // Fees per thousand
    Fees private _buyFees;
    Fees private _sellFees;
    Fees public _fees;
    uint256 public _totalFeesPerThousand;

    // Sell amount of tokens when a sell takes place
    uint256 public _swapThreshold = _totalSupply.mul(1).div(1_000); // 0.1% of supply
    
    DividendsDistributor _dividendsDistributor;
    uint256 private _dividendsDistributorGas = 500_000;
    
    // Cooldown & timer functionality
    bool public _buyCooldownEnabled = true;
    uint8 public _cooldownTimerInterval = 10;
    mapping (address => uint) private _cooldownTimer;
    
    // Other variables
    IDEXRouter public _router;
    address public _pair;
    bool public _tradingOpen = false;
    bool public _swapEnabled = true;
    bool private _inSwap;
    modifier swapping()
    {
        _inSwap = true;
        _;
        _inSwap = false;
    }

    event AutoLiquify(uint256 amountBNB, uint256 amountToken);
    
    /* Token constructor */
    constructor () Auth(msg.sender) {
        _router = IDEXRouter(0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3);
        _pair = IDEXFactory(_router.factory()).createPair(address(this), _router.WETH());
        _allowances[address(this)][address(_router)] = type(uint256).max;
        
        _dividendsDistributor = new DividendsDistributor(address(_router));

        // Buy Fees
        _buyFees.liquidityFeesPerThousand = 20;
        _buyFees.marketingFeesPerThousand = 30;
        _buyFees.teamFeesPerThousand = 20;
        _buyFees.providerFeesPerThousand = 20;
        _buyFees.reflectionFeesPerThousand = 30;
        
        // Sell fees
        _sellFees.liquidityFeesPerThousand = 20;
        _sellFees.marketingFeesPerThousand = 30;
        _sellFees.teamFeesPerThousand = 20;
        _sellFees.providerFeesPerThousand = 20;
        _sellFees.reflectionFeesPerThousand = 30;
        
        // Exempt from fees
        _isFeesExempt[msg.sender] = true;
        
        // Exempt from dividends
        _isDividendsExempt[_pair] = true;
        _isDividendsExempt[address(this)] = true;
        _isDividendsExempt[_deadWallet] = true;
        
        // Exempt from transaction limit
        _isTransactionLimitExempt[msg.sender] = true;
        
        // Exempt from timelock
        _isTimelockExempt[msg.sender] = true;
        _isTimelockExempt[_deadWallet] = true;
        _isTimelockExempt[address(this)] = true;
        _isTimelockExempt[_teamWallet] = true;
        _isTimelockExempt[_providerWallet] = true;
        
        // Set the marketing and liq receiver to the owner as default
        _autoLiquidityReceiver = msg.sender;
        _marketingFeeReceiver = msg.sender;
        
        _balances[msg.sender] = _totalSupply;
        emit Transfer(address(0), msg.sender, _totalSupply);
    }

    receive() external payable { }

    function totalSupply() external view override returns (uint256) { return _totalSupply; }
    function decimals() external pure override returns (uint8) { return _decimals; }
    function symbol() external pure override returns (string memory) { return _symbol; }
    function name() external pure override returns (string memory) { return _name; }
    function getOwner() external view override returns (address) { return _owner; }
    function balanceOf(address account) public view override returns (uint256) { return _balances[account]; }
    function allowance(address holder, address spender) external view override returns (uint256) { return _allowances[holder][spender]; }

    function approve(address spender, uint256 amount) public override returns (bool) {
        _allowances[msg.sender][spender] = amount;
        emit Approval(msg.sender, spender, amount);
        return true;
    }

    function approveMax(address spender) external returns (bool) {
        return approve(spender, type(uint256).max);
    }

    function transfer(address recipient, uint256 amount) external override returns (bool) {
        return _transferFrom(msg.sender, recipient, amount);
    }

    function transferFrom(address sender, address recipient, uint256 amount) external override returns (bool) {
        if (_allowances[sender][msg.sender] != type(uint256).max) {
            _allowances[sender][msg.sender] = _allowances[sender][msg.sender].sub(amount, "Insufficient Allowance");
        }
        return _transferFrom(sender, recipient, amount);
    }

    // Set buy fees
    function setBuyFees(uint256 reflectionFeesPerThousand, uint256 marketingFeesPerThousand, uint256 teamFeesPerThousand, uint256 providerFeesPerThousand, uint256 liquidityFeesPerThousand) external authorized {
        _buyFees.reflectionFeesPerThousand = reflectionFeesPerThousand;
        _buyFees.marketingFeesPerThousand = marketingFeesPerThousand;
        _buyFees.teamFeesPerThousand = teamFeesPerThousand;
        _buyFees.providerFeesPerThousand = providerFeesPerThousand;
        _buyFees.liquidityFeesPerThousand = liquidityFeesPerThousand;
    }

    // Set sell fees
    function setSellFees(uint256 reflectionFeesPerThousand, uint256 marketingFeesPerThousand, uint256 teamFeesPerThousand, uint256 providerFeesPerThousand, uint256 liquidityFeesPerThousand) external authorized {
        _sellFees.reflectionFeesPerThousand = reflectionFeesPerThousand;
        _sellFees.marketingFeesPerThousand = marketingFeesPerThousand;
        _sellFees.teamFeesPerThousand = teamFeesPerThousand;
        _sellFees.providerFeesPerThousand = providerFeesPerThousand;
        _sellFees.liquidityFeesPerThousand = liquidityFeesPerThousand;
    }

    // Switch trading status
    function tradingStatus(bool status) external onlyOwner {
        _tradingOpen = status;
    }
    
    // Enable/disable cooldown between trades
    function cooldownEnabled(bool status, uint8 interval) external authorized {
        _buyCooldownEnabled = status;
        _cooldownTimerInterval = interval;
    }
    
    // Blacklist/unblacklist an address
    function blacklistAddress(address wallet, bool value) external authorized {
        _isBlacklisted[wallet] = value;
    }
    
    // Setting the max wallet in per thousand
     function setMaxWallet(uint256 maxWallPerThousand) external onlyOwner() {
        _maxWalletToken = _totalSupply.mul(maxWallPerThousand).div(1_000);
    }

    // Set max buy transaction
    function setBuyTransactionLimit(uint256 maxBuyTransactionPerThousand) external authorized {
        _maxBuyTransactionAmount = _totalSupply.mul(maxBuyTransactionPerThousand).div(1_000);
    }

    // Set max sell transaction 
    function setSellTransactionLimit(uint256 maxSellTransactionPerThousand) external authorized {
        _maxSellTransactionAmount = _totalSupply.mul(maxSellTransactionPerThousand).div(1_000);
    }

    // Exempt from dividends
    function setIsDividendsExempt(address holder, bool exempt) external authorized {
        require(holder != address(this) && holder != _pair);
        _isDividendsExempt[holder] = exempt;
        if (exempt) {
            _dividendsDistributor.setShare(holder, 0);
        } else {
            _dividendsDistributor.setShare(holder, _balances[holder]);
        }
    }

    // Exempt from fee
    function setIsFeesExempt(address holder, bool exempt) external authorized {
        _isFeesExempt[holder] = exempt;
    }

    // Exempt from max transaction
    function setIsTransactionLimitExempt(address holder, bool exempt) external authorized {
        _isTransactionLimitExempt[holder] = exempt;
    }

    // Exempt from timelock
    function setIsTimelockExempt(address holder, bool exempt) external authorized {
        _isTimelockExempt[holder] = exempt;
    }

    // Set the marketing and liquidity receivers
    function setFeeReceivers(address autoLiquidityReceiver, address marketingFeeReceiver) external authorized {
        _autoLiquidityReceiver = autoLiquidityReceiver;
        _marketingFeeReceiver = marketingFeeReceiver;
    }

    // Set swap settings
    function setSwapSettings(bool enabled, uint256 amount) external authorized {
        _swapEnabled = enabled;
        _swapThreshold = _totalSupply * amount / 1_000; 
    }

    // Send BNB to marketingwallet
    function manualSend() external authorized {
        uint256 contractETHBalance = address(this).balance;
        payable(_marketingFeeReceiver).transfer(contractETHBalance);
    }
    
    // Set criteria for auto distribution
    function setDistributionCriteria(uint256 minPeriod, uint256 minDistribution) external authorized {
        _dividendsDistributor.setDistributionCriteria(minPeriod, minDistribution);
    }
    
    // Let people claim there dividends
    function claimDividends() external {
        _dividendsDistributor.claimDividends(msg.sender);
    }
    
    // Check how much earnings are unpaid
    function getUnpaidEarnings(address shareholder) public view returns (uint256) {
        return _dividendsDistributor.getUnpaidEarnings(shareholder);
    } 

    // Set gas for distributor
    function setDistributorSettings(uint256 gas) external authorized {
        require(gas < 750_000);
        _dividendsDistributorGas = gas;
    }
    
    // Get the circulatingSupply
    function getCirculatingSupply() public view returns (uint256) {
        return _totalSupply.sub(balanceOf(_deadWallet)).sub(balanceOf(_zeroWallet));
    }

    // Main transfer function
    function _transferFrom(address sender, address recipient, uint256 amount) private returns (bool) {
        if (_inSwap) {
            return _basicTransfer(sender, recipient, amount);
        }
        
        // Check if trading is enabled
        if (!_authorizations[sender] && !_authorizations[recipient]) {
            require(_tradingOpen, "Olympia: Trading not enabled yet");
        }
        
        // Check if address is blacklisted
        require(!_isBlacklisted[recipient] && !_isBlacklisted[sender], 'Olympia: Address is blacklisted');
        
        // Check if buying or selling
        bool isBuying = recipient != _pair; 
        
        // Set buy or sell fees
        _setFees(isBuying);
        
        // Check max wallet
        _checkMaxWallet(sender, recipient, amount);
        
        // Check max transaction
        _checkMaxTransaction(sender, amount, recipient, isBuying);
        
        // Check buy cooldown 
        _checkBuyCooldown(sender, recipient);
        
        // Check if we should do the swap
        if (_shouldSwap()) {
            _swap();
        }
        
        //Exchange tokens
        _balances[sender] = _balances[sender].sub(amount, "Olympia: Insufficient Balance");
        
        uint256 amountReceived = _shouldTakeFees(sender) ? _takeFees(sender, amount) : amount;
        _balances[recipient] = _balances[recipient].add(amountReceived);
        
        // Dividends distributor
        if (!_isDividendsExempt[sender]) {
            try _dividendsDistributor.setShare(sender, _balances[sender]) {} catch {}
        }
        
        if (!_isDividendsExempt[recipient]) {
            try _dividendsDistributor.setShare(recipient, _balances[recipient]) {} catch {} 
        }
        
        try _dividendsDistributor.process(_dividendsDistributorGas) {} catch {}
        
        emit Transfer(sender, recipient, amountReceived);
        return true;
    }
    
    // Do a normal transfer
    function _basicTransfer(address sender, address recipient, uint256 amount) private returns (bool) {
        _balances[sender] = _balances[sender].sub(amount, "Olympia: Insufficient Balance");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
        return true;
    }
    
    // Set the correct fees for buying or selling
    function _setFees(bool isBuying) private {
        if (isBuying) {
            _setFeesOnBuy();
        } else {
            _setFeesOnSell();
        }
    }

    function _setFeesOnBuy() private {
        _fees.liquidityFeesPerThousand = _buyFees.liquidityFeesPerThousand;
        _fees.marketingFeesPerThousand = _buyFees.marketingFeesPerThousand;
        _fees.teamFeesPerThousand = _buyFees.teamFeesPerThousand;
        _fees.providerFeesPerThousand = _buyFees.providerFeesPerThousand;
        _fees.reflectionFeesPerThousand = _buyFees.reflectionFeesPerThousand;
        _totalFeesPerThousand = _fees.liquidityFeesPerThousand
            .add(_fees.marketingFeesPerThousand)
            .add(_fees.teamFeesPerThousand)
            .add(_fees.providerFeesPerThousand)
            .add(_fees.reflectionFeesPerThousand);
    }

    function _setFeesOnSell() private {
        _fees.liquidityFeesPerThousand = _sellFees.liquidityFeesPerThousand;
        _fees.marketingFeesPerThousand = _sellFees.marketingFeesPerThousand;
        _fees.teamFeesPerThousand = _sellFees.teamFeesPerThousand;
        _fees.providerFeesPerThousand = _sellFees.providerFeesPerThousand;
        _fees.reflectionFeesPerThousand = _sellFees.reflectionFeesPerThousand;
        _totalFeesPerThousand = _fees.liquidityFeesPerThousand
            .add(_fees.marketingFeesPerThousand)
            .add(_fees.teamFeesPerThousand)
            .add(_fees.providerFeesPerThousand)
            .add(_fees.reflectionFeesPerThousand);
    }
    
    // Check max wallet
    function _checkMaxWallet(address sender, address recipient, uint256 amount) private view {
        if (!_authorizations[sender] &&
            recipient != _owner &&
            recipient != address(this) &&
            recipient != address(_deadWallet) &&
            recipient != _pair &&
            recipient != _autoLiquidityReceiver &&
            recipient != _marketingFeeReceiver &&
            recipient != _teamWallet &&
            recipient != _providerWallet) {
            uint256 heldTokens = balanceOf(recipient);
            require(heldTokens + amount <= _maxWalletToken, "Olympia: Total Holding is currently limited, you can not buy that much");
        }
    }
    
    // Check for max transaction
    function _checkMaxTransaction(address sender, uint256 amount, address recipient, bool isBuying) private view {
        if (recipient != _owner) {
            if (isBuying) {
                require(amount <= _maxBuyTransactionAmount || _isTransactionLimitExempt[sender] || _isTransactionLimitExempt[recipient], "Olympia: Transaction Limit Exceeded");
            } else {
                require(amount <= _maxSellTransactionAmount || _isTransactionLimitExempt[sender] || _isTransactionLimitExempt[recipient], "Olympia: Transaction Limit Exceeded");
            }
        }
    }
    
    // Check buy cooldown
    function _checkBuyCooldown(address sender, address recipient) private {
        if (sender == _pair && _buyCooldownEnabled && !_isTimelockExempt[recipient]) {
            require(_cooldownTimer[recipient] < block.timestamp, "Olympia: Please wait between two buys");
            _cooldownTimer[recipient] = block.timestamp + _cooldownTimerInterval;
        }
    }
    
    // Check if sender is not fees exempt
    function _shouldTakeFees(address sender) private view returns (bool) {
        return !_isFeesExempt[sender];
    }
    
    // Take the normal total Fee
    function _takeFees(address sender, uint256 amount) private returns (uint256) {
        uint256 totalFeeAmount = amount.mul(_totalFeesPerThousand).div(1_000);
        _balances[address(this)] = _balances[address(this)].add(totalFeeAmount);
        emit Transfer(sender, address(this), totalFeeAmount);
        
        return amount.sub(totalFeeAmount);
    }
    
    // Check if we should sell tokens
    function _shouldSwap() private view returns (bool) {
        return msg.sender != _pair && !_inSwap && _swapEnabled && _balances[address(this)] >= _swapThreshold;
    }
    
    // Main swapback to sell tokens for WBNB
    function _swap() private swapping {
        uint256 liquidityFeesPerThousand = _fees.liquidityFeesPerThousand;
        uint256 amountToLiquify = _swapThreshold.mul(liquidityFeesPerThousand).div(_totalFeesPerThousand).div(2);
        uint256 amountToSwap = _swapThreshold.sub(amountToLiquify);
        
        address[] memory path = new address[](2);
        path[0] = address(this);
        path[1] = _router.WETH();
        
        uint256 balanceBefore = address(this).balance;
        
        _router.swapExactTokensForETHSupportingFeeOnTransferTokens(
            amountToSwap,
            0,
            path,
            address(this),
            block.timestamp
        );

        uint256 amountBNB = address(this).balance.sub(balanceBefore);
        uint256 amountBNBLiquidity = amountBNB.mul(liquidityFeesPerThousand).div(2).div(_totalFeesPerThousand);
        uint256 amountBNBMarketing = amountBNB.mul(_fees.marketingFeesPerThousand).div(_totalFeesPerThousand);
        uint256 amountBNBTeam = amountBNB.mul(_fees.teamFeesPerThousand).div(_totalFeesPerThousand);
        uint256 amountBNBProvider = amountBNB.mul(_fees.providerFeesPerThousand).div(_totalFeesPerThousand);
        uint256 amountBNBReflection = amountBNB.mul(_fees.reflectionFeesPerThousand).div(_totalFeesPerThousand);
        
        if (amountToLiquify > 0) {
            _router.addLiquidityETH{value: amountBNBLiquidity}(
                address(this),
                amountToLiquify,
                0,
                0,
                _autoLiquidityReceiver,
                block.timestamp
            );
            emit AutoLiquify(amountBNBLiquidity, amountToLiquify);
        }
        
        if (amountBNBMarketing > 0) {
            (bool successMarketing, /* bytes memory data */) = payable(_marketingFeeReceiver).call{value: amountBNBMarketing, gas: 30_000}("");
            require(successMarketing, "Olympia: Marketing receiver rejected ETH transfer");
        }

        if (amountBNBTeam > 0) {
            (bool successTeam, /* bytes memory data */) = payable(_teamWallet).call{value: amountBNBTeam, gas: 30_000}("");
            require(successTeam, "Olympia: Team receiver rejected ETH transfer");
        }

        if (amountBNBProvider > 0) {
            (bool successProvider, /* bytes memory data */) = payable(_providerWallet).call{value: amountBNBProvider, gas: 30_000}("");
            require(successProvider, "Olympia: Provider receiver rejected ETH transfer");
        }
        
        if (amountBNBReflection > 0) {
            try _dividendsDistributor.deposit{value: amountBNBReflection}() {} catch {}
        }
    }
}
