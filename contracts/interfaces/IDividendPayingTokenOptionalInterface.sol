// SPDX-License-Identifier: MIT

pragma solidity ^0.8.11;

interface IDividendPayingTokenOptionalInterface {
    function withdrawableDividendOf(address owner) external view returns (uint256);
    function withdrawnDividendOf(address owner) external view returns (uint256);
    function accumulativeDividendOf(address owner) external view returns (uint256);
}
